from django.urls import path
from django.contrib.auth import views as auth_views
from .views import (
    IndexView,
    StadiumListView,
    TeamListView,
    NewsListView,
    TeamDetailView,
    NewsDetailView,
    StadiumDetailView,
    register,
    profile_view,
    CommentCreateView,
)

app_name = "premier_league"

urlpatterns = [
    path('accounts/profile/', profile_view, name='profile'),
    path('stadiums/', StadiumListView.as_view(), name='stadium_list'),
    path('stadiums/<int:pk>/', StadiumDetailView.as_view(), name='stadium_detail'),
    path('clubs/', TeamListView.as_view(), name='team_list'),
    path('teams/<int:pk>/', TeamDetailView.as_view(), name='team_detail'),
    path('news/', NewsListView.as_view(), name='news_list'),
    path('news/<int:pk>/', NewsDetailView.as_view(), name='news_detail'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='premier_league:index'), name='logout'),
    path('register/', register, name='register'),
    path('', IndexView.as_view(), name='index'),
    path('news/<int:pk>/comment/', CommentCreateView.as_view(), name='create_comment'),

]
