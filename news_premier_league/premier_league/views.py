from django.views.generic import ListView, DetailView, TemplateView
from .models import Stadium, Team, News, Comments
from django.contrib.auth import login
from django.shortcuts import render, redirect, get_object_or_404
from .forms import CustomUserCreationForm
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import CreateView


class StadiumListView(ListView):
    model = Stadium
    template_name = 'premier_league/stadium_list.html'
    context_object_name = 'stadiums'


class StadiumDetailView(DetailView):
    model = Stadium
    template_name = 'premier_league/stadium_detail.html'
    context_object_name = 'stadium'


class TeamListView(ListView):
    model = Team
    template_name = 'premier_league/team_list.html'
    context_object_name = 'teams'


class TeamDetailView(DetailView):
    model = Team
    template_name = 'premier_league/team_detail.html'
    context_object_name = 'team'


class NewsListView(ListView):
    model = News
    template_name = 'premier_league/news_list.html'
    context_object_name = 'news_list'


class NewsDetailView(DetailView):
    model = News
    template_name = 'premier_league/news_detail.html'
    context_object_name = 'news'


class IndexView(TemplateView):
    template_name = "premier_league/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        news_list = News.objects.all().order_by('-created_at')
        context['news_list'] = news_list
        return context


def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('premier_league:index')
    else:
        form = CustomUserCreationForm()
    return render(request, 'registration/register.html', {'form': form})


@login_required
def profile_view(request):
    user = request.user
    teams = Team.objects.all()
    user_teams = user.teams.all()

    context = {
        'user': user,
        'teams': teams,
        'user_teams': user_teams,
    }
    return render(request, 'profile.html', context)


class CommentCreateView(CreateView):
    model = Comments
    fields = ['content']
    template_name = 'premier_league/comment_form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        news = get_object_or_404(News, pk=self.kwargs['pk'])
        context['news'] = news
        context['comments'] = Comments.objects.filter(news=news)
        return context

    def form_valid(self, form):
        news = get_object_or_404(News, pk=self.kwargs['pk'])
        form.instance.news = news
        form.instance.author = self.request.user
        form.save()
        return redirect('premier_league:news_detail', pk=news.pk)
