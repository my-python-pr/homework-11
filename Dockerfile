                                # Используем базовый образ Python
FROM python:3.10

# Устанавливаем переменные окружения
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Устанавливаем pipenv
RUN pip install pipenv

# Создаем директорию для приложения
WORKDIR /app

# Копируем Pipfile и Pipfile.lock в контейнер
COPY Pipfile Pipfile.lock /app/

# Устанавливаем зависимости
RUN pipenv update

# Копируем остальные файлы в контейнер
COPY . /app/
